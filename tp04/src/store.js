import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
		regions: [],
		countries: [],
		selectedRegion: 'Africa',
		countrySearch: String
    },
    mutations: {
		setRegions(state, regions){
			this.state.regions = regions;
		},
		setCountries(state, countries){
			this.state.countries = countries;
		},
		setSelectedRegion(state, selectedRegion){
			this.state.selectedRegion = selectedRegion;
		},
		setCountrySearch(state, countrySearch){
			this.state.countrySearch = countrySearch;
		}
    }
})
export default store;