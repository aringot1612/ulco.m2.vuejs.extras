import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        jokes: [],
        jokeSearch: String
    },
    mutations: {
        setJokes(state, jokes){
            this.state.jokes = jokes;
        },
        setJokeSearch(state, jokeSearch){
            this.state.jokeSearch = jokeSearch;
        }
    }
})
export default store;
